# Magnetic Field Values Mappers ROS2 Package

This package contains two nodes that perform magnetic field mapping.

* **mapper_tf2_odom.py** - uses *odom* for pose data, along with magnetic field values.

    Can be run by using the command:

        ros2 run mag_mapper_tf2 mapper_odom

* **mapper_tf2.py** - uses *AMCL* for pose data, along with magnetic field values.

    Can be run by using the command:

        ros2 run mag_mapper_tf2 mapper_amcl
