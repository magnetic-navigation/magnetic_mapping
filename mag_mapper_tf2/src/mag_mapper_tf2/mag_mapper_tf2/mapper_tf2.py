# mapper script using AMCL
import csv
from re import A
import pandas as pd
import math

from threading import Thread
from datetime import datetime

# Base class to handle exceptions
from tf2_ros import TransformException

# Stores known frames and offers frame graph requests
from tf2_ros.buffer import Buffer

# Easy way to request and receive coordinate frame transform
from tf2_ros.transform_listener import TransformListener

import rclpy
from rclpy.node import Node

from sensor_msgs.msg import MagneticField  # the message for magnetic field data
from sensor_msgs.msg import Imu  # the message for imu data

from geometry_msgs.msg import PoseWithCovarianceStamped
# import tf_transformations


class dataCollection(Node):

    def __init__(self):
        super().__init__('Datacollection_tf2')

        # Declare and acquire `target_frame` parameter
        self.declare_parameter('target_frame', 'base_link')
        self.target_frame = self.get_parameter(
            'target_frame').get_parameter_value().string_value

        self.from_frame = 'map'
        self.to_frame = self.target_frame

        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)

        self.tf_buffer2 = Buffer()
        self.tf_listener2 = TransformListener(self.tf_buffer2, self)

        # self.subscription = self.create_subscription(
        #     PoseWithCovarianceStamped, 'amcl_pose', self.amcl_callback, 10)
        # # self.subscription  # prevent unused variable warning

        self.subscription = self.create_subscription(
            MagneticField, 'mag_vals', self.mag_callback, 10)

        self.subscription = self.create_subscription(
            Imu, 'imu', self.imu_callback, 10)

        timer_period = 0.1
        self.timer = self.create_timer(timer_period, self.on_timer)

        self.mag_x = 0
        self.mag_y = 0
        self.mag_z = 0
        self.toggle_data_recording = False
        self. start_condition = False
        self.num_stamp = 0

        self.imu_orientation_x = 0
        self.imu_orientation_y = 0
        self.imu_orientation_z = 0
        self.imu_orientation_w = 0
        self.imu_orientation_theta = 0
        self.prev = 0

    def on_timer(self):
        now_tf2 = rclpy.time.Time()
        trans = None
        trans2 = None
        try:
            trans = self.tf_buffer.lookup_transform(
                'map',
                'base_link',
                now_tf2)

            trans_mag = self.tf_buffer.lookup_transform(
                'map',
                'rm3100_mag',
                now_tf2)
            _logger = self.get_logger()

        # def record_data(self, trans):
            _logger = self.get_logger()

            if(self.toggle_data_recording):
                # record first value
                _logger.info(
                    'x = {:.4} y = {:.4}, x = {:.4}, y = {:.4}, z = {:.4}, w = {:.4}'.format(trans.transform.translation.x, trans.transform.translation.y, trans.transform.rotation.x, trans.transform.rotation.y, trans.transform.rotation.z, trans.transform.rotation.w))
                (roll, pitch, yaw) = self.quaternion_to_euler(trans.transform.rotation.x,
                                                              trans.transform.rotation.y, trans.transform.rotation.z, trans.transform.rotation.w)
                _logger.info("orientation = {:.4}".format(yaw))

                orientation_theta = yaw

                _logger.info(
                    ' [!TRANS_MAG!] x = {:.4} y = {:.4}, x = {:.4}, y = {:.4}, z = {:.4}, w = {:.4}'.format(trans_mag.transform.translation.x, trans_mag.transform.translation.y, trans_mag.transform.rotation.x, trans_mag.transform.rotation.y, trans_mag.transform.rotation.z, trans_mag.transform.rotation.w))

                now = datetime.now()
                self.num_stamp += 1
                values = [trans.transform.translation.x, trans.transform.translation.y, self.mag_x, self.mag_y, self.mag_z,
                          trans_mag.transform.translation.x, trans_mag.transform.translation.y,
                          orientation_theta, trans.transform.rotation.x, trans.transform.rotation.y, trans.transform.rotation.z, trans.transform.rotation.w, now, self.num_stamp,
                          self.imu_orientation_x, self.imu_orientation_y, self.imu_orientation_z, self.imu_orientation_w, self.imu_orientation_theta]

                _logger.info("values")
                _logger.info(f"{values}")

                # if (self.toggle_data_recording):
                with open(filename, 'a', encoding='UTF8', newline='') as f:
                    datacsv = csv.writer(f)
                    datacsv.writerow(values)

        except TransformException as ex:
            self.get_logger().info(
                f'Could not transform {self.from_frame} to {self.to_frame}: {ex}')
            return


# call_back function for magnetic recording


    def mag_callback(self, msg):
        # self.get_logger().info('I heard: text')
        _logger = self.get_logger()

        # _logger.info("magnetic x = {:.2}, y = {:.2}, z = {:.2}".format(
        # msg.magnetic_field.x, msg.magnetic_field.y, msg.magnetic_field.z))
        self.mag_x = msg.magnetic_field.x
        self.mag_y = msg.magnetic_field.y
        self.mag_z = msg.magnetic_field.z

    def imu_callback(self, msg):
        self.imu_orientation_x = msg.orientation.x
        self.imu_orientation_y = msg.orientation.y
        self.imu_orientation_z = msg.orientation.z
        self.imu_orientation_w = msg.orientation.w

        (roll_imu, pitch_imu, yaw_imu) = self.quaternion_to_euler(self.imu_orientation_x,
                                                                  self.imu_orientation_y, self.imu_orientation_z, self.imu_orientation_w)
        self.imu_orientation_theta = yaw_imu

    # tf2 here

    # def amcl_callback(self, msg):
    #     orientation_x = msg.pose.pose.orientation.x
    #     orientation_y = msg.pose.pose.orientation.y
    #     orientation_z = msg.pose.pose.orientation.z
    #     orientation_w = msg.pose.pose.orientation.w

    #     (roll, pitch, yaw) = self.quaternion_to_euler(
    #         orientation_x, orientation_y, orientation_z, orientation_w)
    #     orientation_theta = yaw

    #     _logger = self.get_logger()
    #     _logger.debug("OOPLA")
    #     # input("Press enter to continue")
    #     _logger.debug(
    #         'x = {:.4} y = {:.4}'.format(msg.pose.pose.position.x, msg.pose.pose.position.y))
    #     now = datetime.now()
    #     self.num_stamp += 1
    #     values = [msg.pose.pose.position.x, msg.pose.pose.position.y, self.mag_x, self.mag_y, self.mag_z,
    #               orientation_theta, orientation_x, orientation_y, orientation_z, orientation_w, now, self.num_stamp,
    #               self.imu_orientation_x, self.imu_orientation_y, self.imu_orientation_z, self.imu_orientation_w, self.imu_orientation_theta]

    #     # can control this by using a thread and setting this to 1 and 0 to start recording and stop recording
    #     if(self.toggle_data_recording):
    #         _logger.info("values")
    #         _logger.info(f"{values}")

    #     if (self.toggle_data_recording):
    #         with open(filename, 'a', encoding='UTF8', newline='') as f:
    #             datacsv = csv.writer(f)
    #             datacsv.writerow(values)

    def main_toggle_recording(self):
        # check for a particular input
        # check conidtion
        # here and keep checking
        while True:
            if(input("enter S") == 's'):
                self.toggle_data_recording = not self.toggle_data_recording
                # if(self.toggle_data_recording):
                #     self.start_condition = True
                print(self.toggle_data_recording)

    def quaternion_to_euler(self, x, y, z, w):

        # returns (roll, pitch, yaw) ==> (x,y,z) axes - everything in radians (counterclockwise)

        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll_x = math.atan2(t0, t1)

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch_y = math.asin(t2)

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw_z = math.atan2(t3, t4)

        return roll_x, pitch_y, yaw_z  # in radians


def main(args=None):
    print("Enter the file name")
    fname = input()

    global filename
    filename = fname + ".csv"

    df = pd.DataFrame()
    print(df)

    values = ['loc_x', 'loc_y', 'mag_x', 'mag_y',
              'mag_z', 'mag_loc_x', 'mag_loc_y', 'orientation', 'x_amcl', 'y_amcl', 'z_amcl', 'w_amcl', 'TimeStamp', 'point_num', 'x_imu', 'y_imu', 'z_imu', 'w_imu', 'theta_imu']
    with open(filename, 'a', encoding='UTF8', newline='') as f:
        datacsv = csv.writer(f)
        datacsv.writerow(values)

    rclpy.init(args=args)

    data_coll = dataCollection()
    Thread(target=data_coll.main_toggle_recording).start()

    rclpy.spin(data_coll)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    data_coll.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
